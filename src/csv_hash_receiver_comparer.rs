use crate::{
    csv_headers::HeadersParsed,
    csv_parse_result::{CsvByteRecordWithHash, CsvLeftRightParseResult},
    diff_result::{DiffByteRecordFirstRow, DiffByteRecordsIterator},
};
use crossbeam_channel::{Receiver, Sender};

pub struct CsvHashReceiverStreamComparer {
    receiver: Receiver<CsvLeftRightParseResult<CsvByteRecordWithHash>>,
    sender_csv_records_recycle: Sender<csv::ByteRecord>,
    headers: HeadersParsed,
}

impl CsvHashReceiverStreamComparer {
    pub(crate) fn new(
        receiver: Receiver<CsvLeftRightParseResult<CsvByteRecordWithHash>>,
        sender_csv_records_recycle: Sender<csv::ByteRecord>,
    ) -> Self {
        Self {
            receiver,
            sender_csv_records_recycle,
            headers: Default::default(),
        }
    }
    pub fn recv_hashes_and_compare(self) -> DiffByteRecordsIterator {
        DiffByteRecordFirstRow::new(self.receiver, self.sender_csv_records_recycle, self.headers)
            .into_diff_byte_record_iter()
    }
    pub(crate) fn headers(mut self, headers: HeadersParsed) -> Self {
        self.headers = headers;
        self
    }
}
